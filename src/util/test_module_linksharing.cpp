//std/stl
#include <iostream>
#include <experimental/filesystem>
#include <memory>  // unique_ptr
#include <string>
#include <vector>
#include <getopt.h>
#include <bitset>
#include <sstream>
#include <iomanip>
namespace fs = std::experimental::filesystem;

//YARR
#include "logging.h"
#include "Bookkeeper.h"
#include "Rd53b.h"
#include "ScanHelper.h"
#include "SpecController.h"
#include "RawData.h"
#include "LUT_PlainHMapToColRow.h"
#include "LUT_BinaryTreeRowHMap.h"
#include "LUT_BinaryTreeHitMap.h"

//CLI
#include "CLI11.hpp"

//itkpix_dataflow
#include "rd53b_helpers.h"

#define LOGGER(x) spdlog::x

void wait(std::unique_ptr<SpecController>& hw) {
    std::this_thread::sleep_for(std::chrono::microseconds(100));
    while(!hw->isCmdEmpty()) {}
}

void send_reset(std::unique_ptr<SpecController>& hw, std::unique_ptr<Rd53b>& fe, unsigned signal) {
    LOGGER(info)("Sending reset signal to Chip {}: {:x}", fe->getChipId(), 0xffff & signal);
    fe->writeRegister(&Rd53b::GlobalPulseConf, signal);
    fe->writeRegister(&Rd53b::GlobalPulseWidth, 10);
    wait(hw);
    fe->sendGlobalPulse(fe->getChipId());
    wait(hw);
    //wait(hw);
    fe->writeRegister(&Rd53b::GlobalPulseConf, 0);
}

void set_cores(std::unique_ptr<Rd53b>& fe, std::array<uint16_t, 4> cores, bool use_ptot = false) {
    namespace rh = rd53b::helpers;
    rh::set_core_columns(fe, cores);
    if(use_ptot) {
        fe->writeRegister(&Rd53b::PtotCoreColEn0, cores[0]);
        fe->writeRegister(&Rd53b::PtotCoreColEn1, cores[1]);
        fe->writeRegister(&Rd53b::PtotCoreColEn2, cores[2]);
        fe->writeRegister(&Rd53b::PtotCoreColEn3, cores[3]);
    }
}

void set_pixels_enable(std::unique_ptr<Rd53b>& fe, std::vector<std::pair<unsigned, unsigned>> pixel_addresses, bool use_ptot = false) {
    for(auto pix_address : pixel_addresses) {
        auto col = std::get<0>(pix_address);
        auto row = std::get<1>(pix_address);
        LOGGER(warn)("CHIP[{}]: Enabling pix (col,row) = ({},{})", fe->getChipId(), col, row);
        fe->setEn(col, row, use_ptot ? 0 : 1);
        fe->setInjEn(col, row, 1);
        fe->setHitbus(col, row, use_ptot ? 1 : 0);
    } // pix_address
    fe->configurePixels();
}

void write_config(const json& config, std::unique_ptr<Rd53b>& fe) {
    for(auto j: config.items()) {
        fe->writeNamedRegister(j.key(), j.value());
    }
}

int main(int argc, char* argv[]) {

    // user input
    std::string controller_config_file{""};
    std::string primary_connectivity_file{""};
    std::string secondary_connectivity_file{""};
    uint32_t n_triggers{2};
    bool use_ptot{false};
    bool verbose{false};

    CLI::App app;
    app.add_option("--hw", controller_config_file, "Controller JSON file")->required();
    app.add_option("--primary", primary_connectivity_file, "Connectivity JSON file for PRIMARY chip")->required();
    app.add_option("--secondary", secondary_connectivity_file, "Connectivity JSON file for SECONDARY chip")->required();
    app.add_option("--n-triggers", n_triggers, "Number of triggers to send");
    app.add_option("--ptot", use_ptot, "Configure chips to use PToT instead of ToT");
    app.add_option("-v,--verbose", verbose, "Turn on verbose logging"); 
    CLI11_PARSE(app, argc, argv);


    // check that the input files exist and can be found
    fs::path p_controller_config(controller_config_file);
    fs::path p_primary_config(primary_connectivity_file);
    fs::path p_secondary_config(secondary_connectivity_file);

    if(!fs::exists(p_controller_config)) {
        LOGGER(error)("Provided controller config file (=\"{}\") does not exist!", controller_config_file);
        return 1;
    }
    if(!fs::exists(p_primary_config)) {
        LOGGER(error)("Provided register config file for PRIMARY (=\"{}\") does not exist!", primary_connectivity_file);
        return 1;
    }
    if(!fs::exists(p_secondary_config)) {
        LOGGER(error)("Provided register config file for SECONDARY (=\"{}\") does not exist!", secondary_connectivity_file);
        return 1;
    }

    namespace rh = rd53b::helpers;

    // get the Spec hardware controller
    auto hw = rh::spec_init(controller_config_file);

    // handle to the "global" front-end (using the primary config)
    auto fe_global = rh::rd53b_init(hw, primary_connectivity_file);
    fe_global->setChipId(16);

    // get the PRIMARY and SECONDARY front-end objects
    auto fe_primary = rh::rd53b_init(hw, primary_connectivity_file);
    auto cfg_primary = dynamic_cast<Rd53bCfg*>(fe_primary.get());
    auto fe_secondary = rh::rd53b_init(hw, secondary_connectivity_file);
    auto cfg_secondary = dynamic_cast<Rd53bCfg*>(fe_secondary.get());

    // flush all data buffers in the fpga and on the front-ends
    fe_primary->sendClear(fe_primary->getChipId());
    fe_secondary->sendClear(fe_secondary->getChipId());

    // Sync CMD decoder
    hw->setCmdEnable(fe_global->getTxChannel());
    hw->setTrigEnable(0);
    wait(hw);
    hw->flushBuffer();
    hw->setRxEnable(fe_global->getRxChannel());
    hw->setRxEnable(fe_primary->getRxChannel());
    hw->setRxEnable(fe_secondary->getRxChannel());
    hw->runMode();

    //
    // configure the front-ends and disable all pixels
    //
    rh::rd53b_configure(hw, fe_primary);
    rh::disable_pixels(fe_primary);
    wait(hw);
    hw->flushBuffer();


    //std::this_thread::sleep_for(std::chrono::milliseconds(100)); // TODO check if this does anything
    rh::rd53b_configure(hw, fe_secondary);
    rh::disable_pixels(fe_secondary);
    wait(hw);
    hw->flushBuffer();

    //
    // TODO: understand if these RX sync and com checks can be done in link-sharing configuration
    //
    //LOGGER(info)("Checking RX sync for primary...");
    //hw->setCmdEnable(cfg_primary->getTxChannel());
    //hw->setRxEnable(cfg_primary->getRxChannel());
    //hw->checkRxSync();
    //if(fe_primary->checkCom() != 1) {
    //    LOGGER(error)("Can't establish communication with PRIMARY, aborting!");
    //    return 1;
    //}

    //
    // TODO: understand if these RX sync and com checks can be done in link-sharing configuration
    //
    //LOGGER(info)("Checking RX sync for secondary...");
    //hw->setCmdEnable(cfg_secondary->getTxChannel());
    //hw->setRxEnable(cfg_secondary->getRxChannel());
    //hw->checkRxSync();
    //if(fe_secondary->checkCom() != 1) {
    //    LOGGER(error)("Can't establish communication with SECONDARY, aborting!");
    //    return 1;
    //}

    // reset some things (check if this is even needed, or if it's only needed for PRIMARY)
    uint16_t reset_cmd{0xB9}; // see Table 24 in manual
    send_reset(hw, fe_primary, reset_cmd);
    send_reset(hw, fe_secondary, reset_cmd);

    // configure pixels that we want to inject triggers
    std::vector<std::pair<unsigned, unsigned>> pixel_addresses_primary {
        {0,0}
        ,{1,0}
    };
    std::vector<std::pair<unsigned, unsigned>> pixel_addresses_secondary {
        {7,0}
        ,{2,1}
    };

    LOGGER(info)("-----------------------------------------------------");
    LOGGER(info)("Enabling the following pixels (row, col) in PRIMARY:");
    for(const auto& rc: pixel_addresses_primary) {
        LOGGER(info)("  ({}, {})", std::get<0>(rc), std::get<1>(rc));
    }
    LOGGER(info)("-----------------------------------------------------");
    LOGGER(info)("Enabling the following pixels (row, col) in SECONDARY:");
    for(const auto& rc: pixel_addresses_secondary) {
        LOGGER(info)("  ({}, {})", std::get<0>(rc), std::get<1>(rc));
    }
    LOGGER(info)("-----------------------------------------------------");
    

    // enable pixels for digital injection on PRIMARY
    std::array<uint16_t, 4> cores = {0x0, 0x0, 0x0, 0x0};
    set_cores(fe_primary, cores, use_ptot);
    wait(hw);
    if(fe_primary->InjDigEn.read() == 1) {
        LOGGER(info)("Enabling PRIMARY pixels for digital injection");
        set_pixels_enable(fe_primary, pixel_addresses_primary);
        wait(hw);
        // configure the corresponding core columns
        // WARNING: this assumes that our selected pixels are in the first core column
        cores[0] = 0xf;
        set_cores(fe_primary, cores, use_ptot);
        wait(hw);
    }

    // enable pixels for digital injection on SECONDARY
    cores = {0x0, 0x0, 0x0, 0x0};
    set_cores(fe_secondary, cores, use_ptot);
    if(fe_secondary->InjDigEn.read() == 1) {
        LOGGER(info)("Enabling SECONDARY pixels for digital injection");
        set_pixels_enable(fe_secondary, pixel_addresses_secondary);
        wait(hw);
        cores[0] = 0xf;
        // WARNING: this assumes that our selected pixels are in the first core column
        set_cores(fe_secondary, cores, use_ptot);
        wait(hw);
    }

    // configure and start the triggers
    hw->setCmdEnable(cfg_primary->getTxChannel());
    hw->setCmdEnable(cfg_secondary->getTxChannel());
    json trigger_config =  {{"trigMultiplier", 16},
                            {"count", n_triggers},
                            {"delay", 56},
                            {"extTrigger", false},
                            {"frequency", 5000},
                            {"noInject", false},
                            {"time", 0},
                            {"edgeMode", true},
                            {"edgeDuration", 20}};

    int count = trigger_config["count"];
    LOGGER(info)("Trigger config count = {}", count);
    rh::spec_init_trigger(hw, trigger_config);
    wait(hw);

    // begin triggers
    hw->runMode();

    send_reset(hw, fe_primary, reset_cmd);
    wait(hw);
    std::this_thread::sleep_for(std::chrono::milliseconds(100));


    fe_primary->sendClear(fe_primary->getChipId());
    fe_secondary->sendClear(fe_secondary->getChipId());

    wait(hw);
    hw->flushBuffer();
    wait(hw);
    hw->setTrigEnable(0x1);

    if(hw->getTrigEnable() == 0) {
        LOGGER(error)("Trigger is not enabled!");
        throw std::runtime_error("Trigger is not enabled but waiting for triggers!");
    }

    // now readback the data from the FPGA (which groups data in 32-bit blocks)
    uint32_t done = 0;
    RawData* data = nullptr;
    unsigned total_word_count_32 = 0;
    std::vector<uint32_t> data_vec;
    while(done == 0) {
        done = hw->isTrigDone();
        do {
            data = hw->readData();
            if(data != nullptr) {
                for(size_t i = 0; i < data->words; i++) {
                    data_vec.push_back(data->buf[i]);
                } // i
            }
        } while (data != nullptr);
    }
    std::this_thread::sleep_for(hw->getWaitTime());
    do {
        data = hw->readData();
        if(data != nullptr) {
            for(size_t i = 0; i < data->words; i++) {
                data_vec.push_back(data->buf[i]);
            } // i
        }
    } while (data != nullptr);


    // re-group the 32-bit oriented data blocks into 64-bit blocks (AURORA blocks)
    std::vector<uint64_t> blocks;
    for(size_t i =  0; i < data_vec.size(); i+=2) {
        if(blocks.size() > 500) {
            LOGGER(error)("More than 500 blocks, not considering any more!");
            break;
        }
        uint64_t data0 = static_cast<uint64_t>(data_vec.at(i));
        uint64_t data1 = static_cast<uint64_t>(data_vec.at(i+1));
        uint64_t data = data1 | (data0 << 32);
        std::bitset<64> bits(data);
        std::cout << "block[" << std::setw(4) << blocks.size() << "]: " << bits.to_string() << std::endl;
        blocks.push_back(data);
    }

    // TODO: dump the data in "blocks" to file or decode them
    

    return 0;
}
