# itkpix-link-sharing

Code to test link sharing functionality of ITkPix ASICs

# Getting the Code

Clone this repository, with a recursive checkout:

```
$ git clone --recursive https://gitlab.cern.ch/dantrim/itkpix-link-sharing.git
```

# Building and Installing

After [getting the code](#getting-the-code), follow these steps to compile and build the source code:

```
$ cd /path/to/itkpix-link-sharing/
$ source /opt/rh/devtoolset-7/enable
$ cmake3 -DSELECT_LIBS="Rd53b|Spec" -DSELECT_TOOLS="scanConsole" -S . -B build
$ cmake3 --build build --clean-first --parallel $(($(nproc) - 1))
$ cmake3 --build build --target install
```

# Running Module Link Sharing Test

Upong a [successful build](#building-and-installing), a `bin/` directory will
now exist under `/path/to/itkpix-link-sharing` (i.e. `/path/to/itkpix-link-sharing/bin`).
The executable `test_module_linksharing` exists, and you can see how to use it by
calling it with the `--help` flag:

```
$ ./bin/test_module_linksharing --help
Usage: ./bin/test_module_linksharing [OPTIONS]

Options:
  -h,--help                   Print this help message and exit
  --hw TEXT REQUIRED          Controller JSON file
  --primary TEXT REQUIRED     Connectivity JSON file for PRIMARY chip
  --secondary TEXT REQUIRED   Connectivity JSON file for SECONDARY chip
  --n-triggers UINT           Number of triggers to send
  --ptot BOOLEAN              Configure chips to use PToT instead of ToT
  -v,--verbose BOOLEAN        Turn on verbose logging

```

## Options for `test_module_linksharing`
Here is a description of the command line options `test_module_linksharing` takes:

  * `--hw`: The path to the YARR controller configuration JSON file (what you would provide with `-r` to `scanConsole`)
  * `--primary`: The path to the connectivity JSON file for the PRIMARY chip (this is a YARR connectivity JSON file **containing information for a *single* chip only (the PRIMARY chip)**)
  * `--secondary`: The path to the connectivity JSON file for the SECONDARY chip (this is a YARR connectivity JSON file **containing information for a *single* chip only (the SECONDARY chip)**)
  * `--n-triggers`: The number of triggers to send to the chips
  * `--ptot`: Whether or not to use precisition ToT instead of standard ToT (for ITkPixV1.0 you may want precision ToT)

## What is `test_module_linksharing` for?

Currently, `test_module_linksharing` simply takes the configuration files that you provide
(for the controller and the primary and secondary chips),
configures them, sends the specified number of triggers, and dumps the received data
to screen (if there is any).
It assumes that the system under test is composed only of two chips, one of which is the primary
and one of which is the secondary. Support for other link sharing configurations would
need to be added.

There is actually no built-in assumption of link sharing in the script `test_module_linksharing`,
since configuring the ASICs for link sharing is done entirely via the register configuration files
pointed to by the user provided connectivity files (via the `--primary` and `--secondary` commandline options).
All `test_module_linksharing` does is allow for reading back the data from a multi-chip configuration.

Right now, any data received from the chips is simply printed to the screen as 64-bit blocks (AURORA blocks).
Decoding the data, or dumping the data to files (to be parsed/analysed elsewhere), would need to be added.

# Running `scanConsole`

Note that after [building](#building-and-installing), YARR's `scanConsole` is also built
and can be used as usual:

```
$ cd /path/to/itkpix-link-sharing
$ ./bin/scanConsole -h
[16:21:10:237][  info  ][               ]: #####################################
[16:21:10:237][  info  ][               ]: # Welcome to the YARR Scan Console! #
[16:21:10:237][  info  ][               ]: #####################################
[16:21:10:237][  info  ][               ]: -> Parsing command line parameters ...
Help:
 -h: Shows this.
 -n <threads> : Set number of processing threads.
 -s <scan_type> : Scan config
 -c <connectivity.json> [<cfg2.json> ...]: Provide connectivity configuration, can take multiple arguments.
 -r <ctrl.json> Provide controller configuration.
 -t <target_charge> [<tot_target>] : Set target values for threshold/charge (and tot).
 -p: Enable plotting of results.
 -o <dir> : Output directory. (Default ./data/)
 -m <int> : 0 = pixel masking disabled, 1 = start with fresh pixel mask, default = pixel masking enabled
 -k: Report known items (Scans, Hardware etc.)
 -W: Enable using Local DB.
 -d <database.json> : Provide database configuration. (Default /home/dantrim/.yarr/localdb/goosevulture.dhcp.lbl.gov_database.json)
 -i <site.json> : Provide site configuration. (Default /home/dantrim/.yarr/localdb/goosevulture.dhcp.lbl.gov_site.json)
 -u <user.json> : Provide user configuration. (Default /home/dantrim/.yarr/localdb)
 -l <log_cfg.json> : Provide logger configuration.
 -Q: Set QC scan mode.
 -I: Set interactive mode.
 ```


